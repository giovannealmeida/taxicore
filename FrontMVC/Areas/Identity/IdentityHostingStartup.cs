﻿using System;
using FrontMVC.Areas.Identity.Data;
using FrontMVC.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(FrontMVC.Areas.Identity.IdentityHostingStartup))]
namespace FrontMVC.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<FrontMVCContext>(options =>
                    options.UseSqlite(
                        context.Configuration.GetConnectionString("FrontMVCContextConnection")));

                services.AddDefaultIdentity<FrontMVCUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<FrontMVCContext>();
            });
        }
    }
}