﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TaxiDTO.DTO;

namespace FrontMVC.Models
{
    public class ManagerViewModel
    {
        public ManagerViewModel()
        {

        }

        public ManagerViewModel(ManagerDTO manager)
        {
            Id = manager.Id;
            Name = manager.Name;
            Surname = manager.Surname;
            Cpf = manager.Cpf;
            Email = manager.Email;
            Password = manager.Password;
        }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Surname { get; set; }
     
        [Required]
        public string Cpf { get; set; }
        
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
