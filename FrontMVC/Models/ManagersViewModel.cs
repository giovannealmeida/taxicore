﻿using System.Collections.Generic;
using TaxiDTO.DTO;

namespace FrontMVC.Models
{
    public class ManagersViewModel
    {
        public List<ManagerResponse> Managers { get; set; }
        public ManagersViewModel(List<ManagerResponse> managers)
        {
            Managers = managers;
        }
    }
}
