﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontMVC.Models;
using FrontMVC.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaxiDTO.DTO;

namespace FrontMVC.Controllers
{
    public class ManagerController : Controller
    {
        protected readonly IApiClient _apiClient;


        public ManagerController(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        [HttpGet]
        public JsonResult GetManagers()
        {
            ManagersViewModel viewModel = new ManagersViewModel(_apiClient.GetManagersAsync().Result);
            return Json(viewModel);
        }

        [HttpGet]
        public JsonResult GetManager(int id)
        {
            ManagerViewModel viewModel = new ManagerViewModel(_apiClient.GetManagerByIdAsync(id).Result);
            return Json(viewModel);
        }
        // GET: Manager
        public ActionResult Index()
        {
            //Carregados via Ajax
            //ManagersViewModel viewModel = new ManagersViewModel(_apiClient.GetManagersAsync().Result);
            return View();
        }

        // GET: Manager/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Manager/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Manager/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ManagerViewModel manager)
        {
            /*try
            {*/
                ManagerDTO m = new ManagerDTO
                {
                    Name = manager.Name,
                    Surname = manager.Surname,
                    Email = manager.Email,
                    Cpf = manager.Cpf,
                    Password = manager.Password != null ? manager.Password : "",
                    AdministratorId = 1
                };

                if (_apiClient.AddManagerAsync(m).Result)
                {

                }

                return RedirectToAction(nameof(Index));
            /*}
            catch
            {
                return View();
            }*/
        }

        // GET: Manager/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Manager/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Manager/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Manager/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}