

using FrontMVC.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TaxiDTO.DTO;

namespace FrontMVC.Services
{
    public class ApiClient : IApiClient
    {
        private readonly HttpClient _httpClient;

        public ApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri("https://localhost:44369/");
        }

        public async Task<bool> AddManagerAsync(ManagerDTO manager)
        {
            var response = await _httpClient.PostAsJsonAsync("api/v1/Manager", manager);
            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                return false;
            }

            response.EnsureSuccessStatusCode();

            return true;
        }

        public async Task<List<ManagerResponse>> GetManagersAsync()
        {
            var response = await _httpClient.GetAsync("api/v1/Manager");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<List<ManagerResponse>>();
        }

        public async Task<ManagerResponse> GetManagerByIdAsync(int id)
        {
            var response = await _httpClient.GetAsync($"api/v1/Manager/{id}");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<ManagerResponse>();
        }

        /*public async Task PutManagerAsync(ManagerDTO manager)
        {
            return null;
        }

        public async Task<bool> AddManagerAsync(ManagerDTO manager)
        {
            return null;
        }

        public async Task DeleteManagerAsync(int id)
        {
            return null;
        }

        public async Task<List<DriverResponse>> GetDriversAsync()
        {
            return null;
        }

        public async Task<DriverResponse> GetDriverByIdAsync(int id) { return null; }
        public async Task PutDriverAsync(DriverDTO driver) { return null; }
        public async Task<bool> AddDriverAsync(DriverDTO driver) { return null; }
        public async Task DeleteDriverAsync(int id) { return null; }
        public async Task<List<VehicleResponse>> GetVehiclesAsync() { return null; }
        public async Task<VehicleResponse> GetVehicleByIdAsync(int id) { return null; }
        public async Task PutVehicleAsync(VehicleDTO vehicle) { return null; }
        public async Task<bool> AddVehicleAsync(VehicleDTO vehicle) { return null; }
        public async Task DeleteVehicleAsync(int id) { return null; }*/
    }
}