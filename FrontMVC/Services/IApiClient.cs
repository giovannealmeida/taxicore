using FrontMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxiDTO.DTO;

namespace FrontMVC.Services
{
    public interface IApiClient
    {
       Task<List<ManagerResponse>> GetManagersAsync();
       Task<ManagerResponse> GetManagerByIdAsync(int id);
       Task<bool> AddManagerAsync(ManagerDTO manager);
       /*Task PutManagerAsync(ManagerDTO manager);
       Task DeleteManagerAsync(int id);
       
       Task<List<DriverResponse>> GetDriversAsync();
       Task<DriverResponse> GetDriverByIdAsync(int id);
       Task PutDriverAsync(DriverDTO driver);
       Task<bool> AddDriverAsync(DriverDTO driver);
       Task DeleteDriverAsync(int id);

       Task<List<VehicleResponse>> GetVehiclesAsync();
       Task<VehicleResponse> GetVehicleByIdAsync(int id);
       Task PutVehicleAsync(VehicleDTO vehicle);
       Task<bool> AddVehicleAsync(VehicleDTO vehicle);
       Task DeleteVehicleAsync(int id);*/
       
    }
}