using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Context;
using Backend.Models;
using TaxiDTO.DTO;
using Backend.Infrastructure;

namespace Backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly AppDbContext _context;

        public VehicleController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Vehicle
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleResponse>>> GetVehicles()
        {
            return await _context.Vehicles.AsNoTracking()
                                        .Include(v => v.Administrator)
                                        .Include(v => v.Driver)
                                        .Select(v => v.MapVehicleResponse())
                                        .ToListAsync();
        }

        // GET: api/Vehicle/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VehicleResponse>> GetVehicle(int id)
        {
            var vehicle = await _context.Vehicles.AsNoTracking()
                                        .Include(v => v.Administrator)
                                        .Include(v => v.Driver)
                                        .FirstOrDefaultAsync(v => v.Id == id);

            if (vehicle == null)
            {
                return NotFound();
            }

            return vehicle.MapVehicleResponse();
        }

        // PUT: api/Vehicle/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVehicle(int id, Vehicle vehicle)
        {
            var v = await _context.FindAsync<Vehicle>(id);

            if (v == null)
            {
                return NotFound();
            }

            v.Name = vehicle.Name;
            v.Plate = vehicle.Plate;
            v.LastRevisionAt = vehicle.LastRevisionAt;
            v.LastTireChangeAt = vehicle.LastTireChangeAt;
            v.PicturePath = vehicle.PicturePath;
            v.DriverId = vehicle.DriverId;
            _context.Entry(v).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        // POST: api/Vehicle
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<VehicleResponse>> PostVehicle(VehicleDTO vehicle)
        {
            Vehicle v = new Vehicle
            {
                Name = vehicle.Name,
                Plate = vehicle.Plate,
                LastRevisionAt = vehicle.LastRevisionAt,
                LastTireChangeAt = vehicle.LastTireChangeAt,
                PicturePath = vehicle.PicturePath,
                DriverId = vehicle.DriverId
            };

            _context.Vehicles.Add(v);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVehicle", new
            {
                id = vehicle.Id
            }, v.MapVehicleResponse());
        }

        // DELETE: api/Vehicle/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<VehicleResponse>> DeleteVehicle(int id)
        {
            var vehicle = await _context.Vehicles.FindAsync(id);
            if (vehicle == null)
            {
                return NotFound();
            }

            _context.Vehicles.Remove(vehicle);
            await _context.SaveChangesAsync();

            return vehicle.MapVehicleResponse();
        }
    }
}
