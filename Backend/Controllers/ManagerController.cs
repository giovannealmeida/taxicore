using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Context;
using TaxiDTO.DTO;
using Backend.Infrastructure;

namespace Backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ManagerController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ManagerController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Manager
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ManagerResponse>>> GetManagers()
        {
            return await _context.Managers.AsNoTracking()
                                                .Include(m => m.Administrator)
                                                .Include(m => m.Drivers)
                                                .Select(m => m.MapManagerResponse())
                                                .ToListAsync();
        }

        // GET: api/Manager/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ManagerResponse>> GetManager(int id)
        {
            var manager = await _context.Managers.AsNoTracking()
                                    .Include(m => m.Drivers)
                                    .Include(m => m.Administrator)
                                    .FirstOrDefaultAsync(m => m.Id == id);

            if (manager == null)
            {
                return NotFound();
            }

            return manager.MapManagerResponse();
        }

        // PUT: api/Manager/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutManager(int id, ManagerDTO manager)
        {
            var m = await _context.FindAsync<Manager>(id);

            if (m == null)
            {
                return NotFound();
            }

            m.Name = manager.Name;
            m.Surname = manager.Surname;
            m.Cpf = manager.Cpf;
            m.Email = manager.Email;
            m.Password = manager.Password;

            _context.Entry(m).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        // POST: api/Manager
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ManagerResponse>> PostManager(ManagerDTO manager)
        {
            Manager m = new Manager {
                Name = manager.Name,
                Surname = manager.Surname,
                Cpf = manager.Cpf,
                Email = manager.Email,
                Password = manager.Password,
                AdministratorId = manager.AdministratorId
            };

            _context.Managers.Add(m);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetManager", new { id = manager.Id }, m.MapManagerResponse());
        }

        // DELETE: api/Manager/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ManagerResponse>> DeleteManager(int id)
        {
            var manager = await _context.Managers.FindAsync(id);
            if (manager == null)
            {
                return NotFound();
            }

            _context.Managers.Remove(manager);
            await _context.SaveChangesAsync();

            return manager.MapManagerResponse();
        }
    }
}
