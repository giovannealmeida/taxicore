using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Context;
using Backend.Models;
using Backend.Infrastructure;
using TaxiDTO.DTO;

namespace Backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DriverController : ControllerBase
    {
        private readonly AppDbContext _context;

        public DriverController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Driver
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DriverResponse>>> GetDrivers()
        {
            return await _context.Drivers.AsNoTracking()
                                        .Include(d => d.Vehicle)
                                        .Select(d => d.MapDriverResponse())
                                        .ToListAsync();
        }

        // GET: api/Driver/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DriverDTO>> GetDriver(int id)
        {
            var driver = await _context.Drivers.AsNoTracking()
                                        .Include(d => d.Vehicle)
                                        .FirstOrDefaultAsync(v => v.Id == id);

            if (driver == null)
            {
                return NotFound();
            }

            return driver.MapDriverResponse();
        }

        // PUT: api/Driver/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDriver(int id, DriverDTO driver)
        {
            var d = await _context.FindAsync<Driver>(id);

            if (d == null)
            {
                return NotFound();
            }

            d.Name = driver.Name;
            d.Surname = driver.Surname;
            d.Cpf = driver.Cpf;
            d.Email = driver.Email;
            d.Password = driver.Password;

            _context.Entry(d).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        // POST: api/Driver
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<DriverResponse>> PostDriver(DriverDTO driver)
        {
            Driver d = new Driver
            {
                Name = driver.Name,
                Surname = driver.Surname,
                Cpf = driver.Cpf,
                Email = driver.Email,
                Password = driver.Password,
                ManagerId = driver.ManagerId
            };

            _context.Drivers.Add(d);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDriver", new { id = driver.Id }, d.MapDriverResponse());
        }

        // DELETE: api/Driver/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DriverResponse>> DeleteDriver(int id)
        {
            var driver = await _context.Drivers.FindAsync(id);
            if (driver == null)
            {
                return NotFound();
            }

            _context.Drivers.Remove(driver);
            await _context.SaveChangesAsync();

            return driver.MapDriverResponse();
        }
    }
}
