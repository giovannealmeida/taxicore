using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Context;
using Backend.Models;
using TaxiDTO.DTO;
using Backend.Infrastructure;

namespace Backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AdministratorController : ControllerBase
    {
        private readonly AppDbContext _context;

        public AdministratorController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Administrator
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AdministratorResponse>>> GetAdministrators()
        {
            return await _context.Administrators.AsNoTracking()
                                                .Include(a => a.Managers)
                                                .Include(a => a.Vehicles)
                                                .Select(a => a.MapAdministratorResponse())
                                                .ToListAsync();
        }

        // GET: api/Administrator/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AdministratorResponse>> GetAdministrator(int id)
        {
            var administrator = await _context.Administrators.AsNoTracking()
                                                .Include(a => a.Managers)
                                                .Include(a => a.Vehicles)
                                                .FirstOrDefaultAsync(a => a.Id == id);
            if (administrator == null)
            {
                return NotFound();
            }

            return administrator.MapAdministratorResponse();
        }

        // PUT: api/Administrator/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAdministrator(int id, AdministratorDTO administrator)
        {
            var a = await _context.FindAsync<Administrator>(id);

            if (a == null)
            {
                return NotFound();
            }

            a.Name = administrator.Name;
            a.Surname = administrator.Surname;
            a.Cpf = administrator.Cpf;
            a.Email = administrator.Email;
            a.Password = administrator.Password;
            _context.Entry(a).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        // POST: api/Administrator
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<AdministratorResponse>> PostAdministrator(AdministratorDTO administrator)
        {
            Administrator a = new Administrator
            {
                Name = administrator.Name,
                Surname = administrator.Surname,
                Cpf = administrator.Cpf,
                Email = administrator.Email,
                Password = administrator.Password
            };

            _context.Administrators.Add(a);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAdministrator", new { id = administrator.Id }, a.MapAdministratorResponse());
        }

        // DELETE: api/Administrator/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AdministratorResponse>> DeleteAdministrator(int id)
        {
            var administrator = await _context.Administrators.FindAsync(id);
            if (administrator == null)
            {
                return NotFound();
            }

            _context.Administrators.Remove(administrator);
            await _context.SaveChangesAsync();

            return administrator.MapAdministratorResponse();
        }
    }
}
