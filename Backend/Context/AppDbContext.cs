using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Context
{
    public class AppDbContext : DbContext
    {
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();

            modelBuilder.Entity<Administrator>()
                    .Property(s => s.CreatedAt)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Administrator>()
                    .Property(s => s.UpdatedAt)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Manager>()
                    .Property(s => s.CreatedAt)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Manager>()
                    .Property(s => s.UpdatedAt)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Driver>()
                    .Property(s => s.CreatedAt)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Driver>()
                    .Property(s => s.UpdatedAt)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Vehicle>()
                    .Property(s => s.CreatedAt)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Vehicle>()
                    .Property(s => s.UpdatedAt)
                    .HasDefaultValueSql("GETDATE()");



        }
    }
}