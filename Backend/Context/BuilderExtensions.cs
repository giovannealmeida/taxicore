using System;
using System.Security.Cryptography;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Context
{
    public static class BuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Administrator>().HasData(
                new Administrator
                {
                    Id = 1,
                    Name = "Administrator",
                    Cpf = "11111111111",
                    Email = "admin@email.com",
                    Password = "dummy"
                }
            );

            modelBuilder.Entity<Manager>().HasData(
                new Manager
                {
                    Id = 1,
                    Name = "Manager",
                    Cpf = "22222222222",
                    Email = "manager@email.com",
                    Password = "dummy",
                    AdministratorId = 1
                }
            );
        }
    }
}