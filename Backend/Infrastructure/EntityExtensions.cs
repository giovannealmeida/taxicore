using System.Linq;
using Backend.Models;
using TaxiDTO.DTO;

namespace Backend.Infrastructure
{
    public static class EntityExtensions
    {
        public static AdministratorResponse MapAdministratorResponse(this Administrator administrator) =>
             new AdministratorResponse
             {
                 Id = administrator.Id,
                 Name = administrator.Name,
                 Surname = administrator.Surname,
                 Email = administrator.Email,
                 Cpf = administrator.Cpf,
                 Managers = administrator.Managers?
                    .Select(m => new ManagerDTO
                    {
                        Id = m.Id,
                        Name = m.Name,
                        Surname = m.Surname,
                        Cpf = m.Cpf,
                        Email = m.Email
                    })
                    .ToList(),
                 Vehicles = administrator.Vehicles?
                    .Select(v => new VehicleDTO
                    {
                        Id = v.Id,
                        Name = v.Name,
                        Plate = v.Plate,
                        PicturePath = v.PicturePath,
                        LastTireChangeAt = v.LastTireChangeAt,
                        LastRevisionAt = v.LastRevisionAt
                    })
                    .ToList()
             };

        public static ManagerResponse MapManagerResponse(this Manager manager)
        {
            ManagerResponse response = new ManagerResponse
            {
                Id = manager.Id,
                Name = manager.Name,
                Surname = manager.Surname,
                Email = manager.Email,
                Cpf = manager.Cpf,
                AdministratorId = manager.AdministratorId,
                Drivers = manager.Drivers?
                   .Select(d => new DriverDTO
                   {
                       Id = d.Id,
                       Name = d.Name,
                       Surname = d.Surname,
                       Cpf = d.Cpf,
                       Email = d.Email
                   })
                   .ToList()
            };

            if(manager.Administrator != null)
            {
                response.Administrator = new AdministratorDTO
                {
                    Id = manager.Administrator.Id,
                    Name = manager.Administrator.Name,
                    Surname = manager.Administrator.Surname,
                    Email = manager.Administrator.Email,
                    Cpf = manager.Administrator.Cpf
                };
            }

            return response;
        }

        public static DriverResponse MapDriverResponse(this Driver driver)
        {
            var response = new DriverResponse
            {
                Id = driver.Id,
                Name = driver.Name,
                Surname = driver.Surname,
                Email = driver.Email,
                Cpf = driver.Cpf,
                ManagerId = driver.ManagerId
            };

            if (driver.Vehicle != null)
            {
                response.Vehicle = new VehicleDTO
                {
                    Id = driver.Vehicle.Id,
                    Name = driver.Vehicle.Name,
                    Plate = driver.Vehicle.Plate,
                    PicturePath = driver.Vehicle.PicturePath,
                    LastRevisionAt = driver.Vehicle.LastRevisionAt,
                    LastTireChangeAt = driver.Vehicle.LastTireChangeAt
                };
            }

            return response;
        }

        public static VehicleResponse MapVehicleResponse(this Vehicle vehicle)
        {
            var response = new VehicleResponse
            {
                Id = vehicle.Id,
                Name = vehicle.Name,
                Plate = vehicle.Plate,
                LastRevisionAt = vehicle.LastRevisionAt,
                LastTireChangeAt = vehicle.LastTireChangeAt,
                PicturePath = vehicle.PicturePath,
                DriverId = vehicle.DriverId,
                AdministratorId = vehicle.AdministratorId
            };

            if (vehicle.Driver != null)
            {
                response.Driver = new DriverDTO
                {
                    Id = vehicle.Driver.Id,
                    Name = vehicle.Driver.Name,
                    Surname = vehicle.Driver.Surname,
                    Email = vehicle.Driver.Email,
                    Cpf = vehicle.Driver.Cpf,
                    ManagerId = vehicle.Driver.ManagerId
                };
            }

            if (vehicle.Administrator != null)
            {
                response.Administrator = new AdministratorDTO
                {
                    Id = vehicle.Administrator.Id,
                    Name = vehicle.Administrator.Name,
                    Surname = vehicle.Administrator.Surname,
                    Email = vehicle.Administrator.Email,
                    Cpf = vehicle.Administrator.Cpf
                };
            }

            return response;
        }
    }
}