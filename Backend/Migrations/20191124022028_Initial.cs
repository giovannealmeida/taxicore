﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Backend.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tbl_admin",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_at = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    updated_at = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    name = table.Column<string>(maxLength: 20, nullable: false),
                    surname = table.Column<string>(maxLength: 20, nullable: true),
                    cpf = table.Column<string>(maxLength: 11, nullable: false),
                    email = table.Column<string>(maxLength: 60, nullable: false),
                    password = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_admin", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "tbl_manager",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_at = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    updated_at = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    name = table.Column<string>(maxLength: 20, nullable: false),
                    surname = table.Column<string>(maxLength: 20, nullable: true),
                    cpf = table.Column<string>(maxLength: 11, nullable: false),
                    email = table.Column<string>(maxLength: 60, nullable: false),
                    password = table.Column<string>(nullable: false),
                    administrator_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_manager", x => x.id);
                    table.ForeignKey(
                        name: "FK_tbl_manager_tbl_admin_administrator_id",
                        column: x => x.administrator_id,
                        principalTable: "tbl_admin",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tbl_driver",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_at = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    updated_at = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    name = table.Column<string>(maxLength: 20, nullable: false),
                    surname = table.Column<string>(maxLength: 20, nullable: true),
                    cpf = table.Column<string>(maxLength: 11, nullable: false),
                    email = table.Column<string>(maxLength: 60, nullable: false),
                    password = table.Column<string>(nullable: false),
                    manager_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_driver", x => x.id);
                    table.ForeignKey(
                        name: "FK_tbl_driver_tbl_manager_manager_id",
                        column: x => x.manager_id,
                        principalTable: "tbl_manager",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tbl_vehicle",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_at = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    updated_at = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    name = table.Column<string>(maxLength: 20, nullable: true),
                    plate = table.Column<string>(nullable: false),
                    last_revision_at = table.Column<DateTime>(nullable: false),
                    last_tire_change_at = table.Column<DateTime>(nullable: false),
                    picture_path = table.Column<string>(nullable: true),
                    driver_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_vehicle", x => x.id);
                    table.ForeignKey(
                        name: "FK_tbl_vehicle_tbl_driver_driver_id",
                        column: x => x.driver_id,
                        principalTable: "tbl_driver",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "tbl_admin",
                columns: new[] { "id", "cpf", "email", "name", "password", "surname" },
                values: new object[] { 1, "11111111111", "admin@email.com", "Administrator", "dummy", null });

            migrationBuilder.InsertData(
                table: "tbl_manager",
                columns: new[] { "id", "administrator_id", "cpf", "email", "name", "password", "surname" },
                values: new object[] { 1, 1, "22222222222", "manager@email.com", "Manager", "dummy", null });

            migrationBuilder.CreateIndex(
                name: "IX_tbl_driver_manager_id",
                table: "tbl_driver",
                column: "manager_id");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_manager_administrator_id",
                table: "tbl_manager",
                column: "administrator_id");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_vehicle_driver_id",
                table: "tbl_vehicle",
                column: "driver_id",
                unique: true,
                filter: "[driver_id] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tbl_vehicle");

            migrationBuilder.DropTable(
                name: "tbl_driver");

            migrationBuilder.DropTable(
                name: "tbl_manager");

            migrationBuilder.DropTable(
                name: "tbl_admin");
        }
    }
}
