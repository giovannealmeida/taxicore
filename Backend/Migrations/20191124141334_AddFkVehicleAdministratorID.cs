﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Backend.Migrations
{
    public partial class AddFkVehicleAdministratorID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "admin_id",
                table: "tbl_vehicle",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_tbl_vehicle_admin_id",
                table: "tbl_vehicle",
                column: "admin_id");

            migrationBuilder.AddForeignKey(
                name: "FK_tbl_vehicle_tbl_admin_admin_id",
                table: "tbl_vehicle",
                column: "admin_id",
                principalTable: "tbl_admin",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbl_vehicle_tbl_admin_admin_id",
                table: "tbl_vehicle");

            migrationBuilder.DropIndex(
                name: "IX_tbl_vehicle_admin_id",
                table: "tbl_vehicle");

            migrationBuilder.DropColumn(
                name: "admin_id",
                table: "tbl_vehicle");
        }
    }
}
