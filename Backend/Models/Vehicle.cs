using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaxiDTO.DTO;

namespace Backend.Models
{
    [Table("tbl_vehicle")]
    public class Vehicle : VehicleDTO
    {
        [ForeignKey("DriverId")]
        public Driver Driver { get; set; }

        [ForeignKey("AdministratorId")]
        public Administrator Administrator { get; set; }
    }
}