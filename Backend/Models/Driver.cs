using System.ComponentModel.DataAnnotations.Schema;
using TaxiDTO.DTO;

namespace Backend.Models
{
    [Table("tbl_driver")]
    public class Driver : DriverDTO
    {
        [ForeignKey("ManagerId")]
        public Manager Manager { get; set; }

        public Vehicle Vehicle { get; set; }
    }
}