using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TaxiDTO.DTO;

namespace Backend.Models
{
    [Table("tbl_admin")]
    public class Administrator : AdministratorDTO
    {
        public IEnumerable<Manager> Managers { get; set; }
        public IEnumerable<Vehicle> Vehicles { get; set; }
    }
}