using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using TaxiDTO.DTO;

namespace Backend.Models
{
    [Table("tbl_manager")]
    public class Manager : ManagerDTO
    {
        [ForeignKey("AdministratorId")]
        public virtual Administrator Administrator { get; set; }

        public virtual IEnumerable<Driver> Drivers { get; set; }
    }
}