using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiDTO.DTO
{
    public class BaseModelDTO
    {
        [Column("id")]
        public int Id { get; set; }
        
        [Required]
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }
    }
}