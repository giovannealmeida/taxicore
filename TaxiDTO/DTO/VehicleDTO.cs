using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiDTO.DTO
{
    public class VehicleDTO : BaseModelDTO
    {
        [StringLength(20)]
        [Column("name")]
        public string Name { get; set; }

        [Required]
        [Column("plate")]
        public string Plate { get; set; }

        [Required]
        [Column("last_revision_at")]
        public DateTime LastRevisionAt { get; set; }

        [Required]
        [Column("last_tire_change_at")]
        public DateTime LastTireChangeAt { get; set; }

        [Column("picture_path")]
        public string PicturePath { get; set; }

        [Column("driver_id")]
        public int? DriverId { get; set; }

        [Column("admin_id")]
        public int AdministratorId { get; set; }
    }
}