using System.Collections.Generic;

namespace TaxiDTO.DTO
{
    public class AdministratorResponse : AdministratorDTO
    {
        public IEnumerable<ManagerDTO> Managers { get; set; }
        public IEnumerable<VehicleDTO> Vehicles { get; set; }
    }
}