using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiDTO.DTO
{
    public class ManagerDTO : UserDTO
    {
        [Required]
        [Column("administrator_id")]
        public int AdministratorId { get; set; }
    }
}