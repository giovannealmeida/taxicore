namespace TaxiDTO.DTO
{
    public class DriverResponse : DriverDTO
    {
        public VehicleDTO Vehicle { get; set; }
    }
}