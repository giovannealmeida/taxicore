using System.Collections.Generic;

namespace TaxiDTO.DTO
{
    public class ManagerResponse : ManagerDTO
    {
        public virtual AdministratorDTO Administrator { get; set; }

        public virtual IEnumerable<DriverDTO> Drivers { get; set; }
    }
}