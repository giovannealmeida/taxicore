using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiDTO.DTO
{
    public class DriverDTO : UserDTO
    {
        [Required]
        [Column("manager_id")]
        public int ManagerId { get; set; }
    }
}