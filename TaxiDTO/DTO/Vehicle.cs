namespace TaxiDTO.DTO
{
    public class VehicleResponse : VehicleDTO
    {
        public DriverDTO Driver { get; set; }
        public AdministratorDTO Administrator { get; set; }
    }
}