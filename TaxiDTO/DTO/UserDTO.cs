using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiDTO.DTO
{
    public class UserDTO : BaseModelDTO
    {
        [Required]
        [StringLength(20)]
        [Column("name")]
        public string Name { get; set; }

        [StringLength(20)]
        [Column("surname")]
        public string Surname { get; set; }

        [Required]
        [StringLength(11)]
        [Column("cpf")]
        public string Cpf { get; set; }
        
        [Required]
        [StringLength(60)]
        [Column("email")]
        public string Email { get; set; }

        [Required]
        [Column("password")]
        public string Password { get; set; }
    }
}